import React, {useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router'
import {Link} from 'react-router-dom'
import {Card, Form, Button, Container, Row, Col} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import { Formik, ErrorMessage} from "formik";
import * as Yup from "yup";

function CreateProductCategory(){
    const history = useHistory();

    const [validation, setValidation] = useState([]);
    const token = localStorage.getItem("token");

    const initialValues = {
        name: '',
        description: ''
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Name is required'),
        description: Yup.string()
            .required('Description is required')
    });

    const createProductCategory = async (e) => {
        e.preventDefault();

        const formData = new FormData()

        formData.append('name', e.target.name.value)
        formData.append('description', e.target.description.value)
        console.log(formData)
        
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.post(`http://localhost:8000/api/product-categories/create`, formData).then(({data})=>{
        Swal.fire({
            icon:"success",
            text:"Data has been created !"
        })

        history.push('/product-categories');
        
        }).catch(({response})=>{
            if(response.status===422){
                setValidation(response.data.errors)
            }else{
                Swal.fire({
                    text:"Create data failed !",
                    icon:"error"
                })
            }
        })
    }

    return(
        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4 mb-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                Create Product Category
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                            <Formik 
                                initialValues={initialValues} 
                                validationSchema={validationSchema}
                            >
                            {({ errors, touched, values, handleChange, handleBlur}) => {
                                console.log(values)
                                return (
                                    <Form onSubmit={createProductCategory}>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Name<small className="text-danger">*</small></Form.Label>
                                            <Form.Control name="name" controlid="name" type="text" value={values.name} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} autoComplete="off"/>
                                            <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Description<small className="text-danger">*</small></Form.Label>
                                            <Form.Control as="textarea" controlid="description" name="description" rows={3} value={values.description} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.description && touched.description ? ' is-invalid' : '')}/>
                                            <ErrorMessage name="description" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Button variant="success" type="submit">Save</Button>
                                        <Link className='btn btn-danger' to={"/product-categories"} style={{ marginLeft: 2 }}>
                                            Cancel
                                        </Link>
                                    </Form>
                                );
                            }}
                            </Formik>
                                
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default CreateProductCategory;
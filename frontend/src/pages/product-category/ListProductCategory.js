import React, {useEffect, useState} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import {Card, Table, Container, Row, Col, Button} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import { FaEdit, FaTrash } from "react-icons/fa";

function ListProductCategory(){
    
    const [categories, setCategory] = useState([]);
    const token = localStorage.getItem("token");

    useEffect(()=>{
        fetchData();
    }, [])

    const fetchData = async () => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get("http://localhost:8000/api/product-categories")
        .then((response) => {
            setCategory(response.data[0]);
        })
    }

    const deleteProductCategory = async (id) => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            return result.isConfirmed
        });

        if(!isConfirm){
            return;
        }

        await axios.delete(`http://localhost:8000/api/product-categories/delete/${id}`).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:"Data has been Deleted !"
            })
            fetchData()
        }).catch(({response:{data}})=>{
            Swal.fire({
                text:"Delete Data failed !",
                icon:"error"
            })
        })
    }
    return(

        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                List Product Category 
                                <Link className='btn btn-primary mb-2 float-end' to={"/product-categories/create"}>
                                    Add Product Category
                                </Link>
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                                <Table responsive className="mt4" hover bordered>
                                    <thead>
                                        <tr>
                                            <th className="text-center">No</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            categories.length > 0 && (
                                                categories.map((item, key) => (
                                                    <tr key={key}>
                                                        <td className="text-center">{key + 1}</td>
                                                        <td>{item.name}</td>
                                                        <td>{item.description}</td>
                                                        <td className="text-center">
                                                            <Link to={`/product-categories/edit/${item.id}`} className='btn btn-warning me-2' data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit">
                                                                <FaEdit />  
                                                            </Link>
                                                            <Button variant="danger" onClick={()=>deleteProductCategory(item.id)} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete">
                                                                <FaTrash />  
                                                            </Button>
                                                        </td>
                                                    </tr>               
                                                ))
                                            )
                                        }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default ListProductCategory;
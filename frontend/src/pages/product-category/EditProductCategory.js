import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router'
import {useParams, Link} from 'react-router-dom'
import {Card, Form, Alert, Button, Container, Row, Col} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import { Formik, ErrorMessage, Field } from "formik";
import * as Yup from "yup";

function EditProductCategory(){
    const history = useHistory();   
    const { id } = useParams()

    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [validation,setValidation] = useState([])
    const token = localStorage.getItem("token");

    const initialValues = {
        name: '',
        description: ''
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Name is required'),
        description: Yup.string()
            .required('Description is required')
    });

    useEffect(()=>{
        fetchProductCategory()
    },[])
    
    const fetchProductCategory = async () => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get(`http://localhost:8000/api/product-categories/show/${id}`).then((response)=>{
            const { name, description } = response.data.response.data
            setName(name)
            setDescription(description)            
        }).catch(({response})=>{
            Swal.fire({
                text:response.data.message,
                icon:"error"
            })
        })
    }

    const updateProductCategory = async (e) => {
        e.preventDefault();

        const formData = new FormData()
        formData.append('name', e.target.name.value)
        formData.append('description', e.target.description.value)
        console.log(formData)

        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.post(`http://localhost:8000/api/product-categories/update/${id}`, formData).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:"Data has been edited !"
            })

            history.push("/product-categories")
        }).catch(({response})=>{
            if(response.status === 422){
                setValidation(response.data.errors)
            }else{
                Swal.fire({
                    text:response.data.message,
                    icon:"error"
                })
            }
        })
    }

    return(
        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4 mb-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                Edit Product Category
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                                <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={updateProductCategory}>
                                {({ errors, touched}) => {
                                    return (
                                        <form onSubmit={updateProductCategory}>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Name<small className="text-danger">*</small></Form.Label>
                                                <Field type="text" name="name" value={name} onChange={(event) => setName(event.target.value)} className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} autoComplete="off"/>
                                                <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                                <Form.Label>Description<small className="text-danger">*</small></Form.Label>
                                                <Field as="textarea" name="description" rows={3} value={description} onChange={(event) => setDescription(event.target.value)} className={'form-control' + (errors.description && touched.description ? ' is-invalid' : '')}/>
                                                <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Button variant="warning" type="submit">Update</Button>
                                            <Link className='btn btn-danger' to={"/product-categories"} style={{ marginLeft: 2 }}>
                                                Cancel
                                            </Link>
                                        </form>
                                        );
                                    }}
                                </Formik>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default EditProductCategory;
import React, {useState} from 'react'
import {useHistory} from 'react-router'
import axios from 'axios';
import {Link} from 'react-router-dom'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Formik, ErrorMessage} from "formik";
import * as Yup from "yup";

function Register(){

    //definisi state
    const [validation, setValidation] = useState([]);
    const history = useHistory();

    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirm_password: ''
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
          .min(2, "Mininum 2 characters")
          .required("Name is required !"),
        email: Yup.string()
          .email("Invalid email format")
          .required("Email is required !"),
        password: Yup.string()
          .min(8, "Minimum 8 characters")
          .required("password is required !"),
        confirm_password: Yup.string()
          .oneOf([Yup.ref("password")], "Password's not match")
          .required("Password Confirmation is required !")
    });

    const registerHandler = async (e) =>{
        e.preventDefault();

        const formData = new FormData();
        
        formData.append('name', e.target.name.value);
        formData.append('email', e.target.email.value);
        formData.append('password', e.target.password.value);
        formData.append('password_confirmation', e.target.confirm_password.value);
        console.log(formData)
        await axios.post('http://localhost:8000/api/register', formData)
        .then(() =>{
            Swal.fire({
                icon:"success",
                text:"Register Success !"
            })
            history.push('/');
        })
        .catch((error) =>{
            setValidation(error.response.data)
        })

    }

    return(
        <div className="container" style={{marginTop: "120px"}}>
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card border-0 rounded shadow-sm">
                        <div className="card-body">
                            <h4>Register</h4>
                            <Formik 
                                initialValues={initialValues} 
                                validationSchema={validationSchema}
                            >
                            {({ errors, touched, values, handleChange, handleBlur}) => {
                                return (
                                        <Form onSubmit={registerHandler}>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Name</Form.Label>
                                                <Form.Control type="text" name="name" value={values.name} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} autoComplete="off"/>
                                                <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control type="email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} autoComplete="off"/>
                                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Password</Form.Label>
                                                <Form.Control type="password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')}/>
                                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Password Confirmation</Form.Label>
                                                <Form.Control type="password" name="confirm_password" value={values.confirm_password} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.confirm_password && touched.confirm_password ? ' is-invalid' : '')}/>
                                                <ErrorMessage name="confirm_password" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Button variant="success" type="submit">Register</Button>
                                            <Link className='btn btn-danger' to={"/"} style={{ marginLeft: 2 }}>
                                                Cancel
                                            </Link>
                                        </Form>
                                );
                            }}
                            </Formik>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Register;
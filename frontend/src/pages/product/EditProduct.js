import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router'
import {useParams, Link} from 'react-router-dom'
import {Card, Form, Alert, Button, Container, Row, Col} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import { useFormik } from "formik";
import * as Yup from "yup";

function EditProduct(){
    const history = useHistory();   
    const { id } = useParams()

    const [name, setName] = useState("")
    const [product_category_id, setProductCategory] = useState("")
    const [image, setImage] = useState(null)
    const [weight, setWeight] = useState("")
    const [price, setPrice] = useState("")
    const [stock, setStock] = useState("")
    const [description, setDescription] = useState("")
    const [validation,setValidation] = useState([])
    const [categories, setCategory] = useState([]);
    const token = localStorage.getItem("token");

    useEffect(()=>{
        const fetchCategory = async () => {
            axios.defaults.headers.common['Authorization'] = 'Bearer' + token
    
            await axios.get("http://localhost:8000/api/product-categories")
            .then((response) => {
                setCategory(response.data[0]);
            })
        }

        const fetchProduct = async () => {
            axios.defaults.headers.common['Authorization'] = 'Bearer' + token
    
            await axios.get(`http://localhost:8000/api/products/show/${id}`).then((response)=>{
                const { name, product_category_id, image, weight, price, stock, description } = response.data.response.data
                setName(name)
                setProductCategory(product_category_id)
                setImage(image)
                setWeight(weight)
                setPrice(parseInt(price))       
                setStock(stock)
                setDescription(description)
            }).catch(({response})=>{
                Swal.fire({
                    text:response.data.message,
                    icon:"error"
                })
            })
        }

        fetchProduct()
        fetchCategory()
    },[])

    const changeHandler = (event) => {
		setImage(event.target.files[0]);
	}

    const updateProduct = async (e) => {
        e.preventDefault();

        const formData = new FormData()
        formData.append('name', name)
        formData.append('product_category_id', product_category_id)
        formData.append('weight', weight)
        formData.append('price', price)
        formData.append('stock', stock)
        formData.append('description', description)
        
        if(image!==null){
            formData.append('image', image)
        }

        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.post(`http://localhost:8000/api/products/update/${id}`, formData).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:"Data has been edited !"
            })

            history.push("/products")
        }).catch(({response})=>{
            if(response.status === 422){
                setValidation(response.data.errors)
            }else{
                Swal.fire({
                    text:response.data.message,
                    icon:"error"
                })
            }
        })
    }

    return(
        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4 mb-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                Edit Category Product
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                                <form onSubmit={updateProduct}>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Name</Form.Label>
                                        <Form.Control type="text" value={name} onChange={(event) => setName(event.target.value)}/>
                                        {
                                            validation.name && (
                                                <Alert variant="danger">
                                                    {validation.name[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Category</Form.Label>
                                        <Form.Select aria-label="Default select example" value={product_category_id} onChange={(event) => setProductCategory(event.target.value)}>
                                            <option>Choose Category</option>
                                            { categories.map((item, key) => (
                                                <option key={key} value={item.id} defaultValue={product_category_id == item.id}>
                                                    {item.name}
                                                </option>
                                            ))}
                                        </Form.Select>
                                        {
                                            validation.name && (
                                                <Alert variant="danger">
                                                    {validation.name[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Image</Form.Label>
                                        <Form.Control type="file" onChange={changeHandler} />
                                        {
                                            validation.image && (
                                                <Alert variant="danger">
                                                    {validation.image[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Weight</Form.Label>
                                        <Form.Control type="text" value={weight} onChange={(event) => setWeight(event.target.value)}/>
                                        {
                                            validation.weight && (
                                                <Alert variant="danger">
                                                    {validation.weight[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control type="text" value={price} onChange={(event) => setPrice(event.target.value)}/>
                                        {
                                            validation.price && (
                                                <Alert variant="danger">
                                                    {validation.price[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                        <Form.Label>Stock</Form.Label>
                                        <Form.Control type="text" value={stock} onChange={(event) => setStock(event.target.value)}/>
                                        {
                                            validation.stock && (
                                                <Alert variant="danger">
                                                    {validation.stock[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Description</Form.Label>
                                        <Form.Control as="textarea" rows={3} value={description} onChange={(event) => setDescription(event.target.value)}/>
                                        {
                                            validation.description && (
                                                <Alert variant="danger">
                                                    {validation.description[0]}
                                                </Alert>
                                            )
                                        }
                                    </Form.Group>
                                    <Form.Group className="mb-4">
                                        <Button variant="warning" type="submit">Update</Button>
                                        <Link className='btn btn-danger' to={"/products"} style={{ marginLeft: 2 }}>
                                            Cancel
                                        </Link>
                                    </Form.Group>
                                </form>
                            </Col>
                        </Row>
                    </Card.Body>    
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default EditProduct;
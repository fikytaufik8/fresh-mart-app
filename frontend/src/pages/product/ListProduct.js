import React, {useEffect, useState} from 'react'
import axios from 'axios'
import {Link} from 'react-router-dom'
import {Table, Container, Row, Col, Button, Card} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import NumberFormat  from 'react-number-format'
import { FaEdit, FaTrash } from "react-icons/fa";

function ListProduct(){
    
    const [products, setProducts] = useState([])
    const token = localStorage.getItem("token");

    useEffect(()=>{
        fetchData() 
    },[])

    const fetchData = async () => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get(`http://localhost:8000/api/products`).then(({data})=>{
            setProducts(data[0])
        })
    }

    const deleteProduct = async (id) => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            return result.isConfirmed
        });

        if(!isConfirm){
            return;
        }

        await axios.delete(`http://localhost:8000/api/products/delete/${id}`).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:"Data has been Deleted !"
            })
            fetchData()
        }).catch(({response:{data}})=>{
            Swal.fire({
                text:data.message,
                icon:"error"
            })
        })
    }

    return(
        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                List Product 
                                <Link className='btn btn-primary mb-2 float-end' to={"/products/create"}>
                                    Add Product
                                </Link>
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                                <Table responsive className="mt4" bordered hover>
                                    <thead>
                                        <tr>
                                            <th className="text-center">No</th>
                                            <th className="text-center">Image</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Information</th>
                                            <th>Description</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            products.length > 0 && (
                                                products.map((row, key) => (
                                                    <tr key={key}>
                                                        <td className="text-center">{key + 1}</td>
                                                        <td className="text-center">
                                                            <img width="50px" src={`http://localhost:8000/storage/${row.image}`} />
                                                        </td>
                                                        <td>{row.name}</td>
                                                        <td>{row.product_category.name}</td>
                                                        <td>
                                                            <strong>Weight : </strong> {row.weight} <br/>
                                                            <strong>Price : </strong> <NumberFormat value={row.price} displayType={"text"} thousandSeparator={true} prefix={"Rp"}/> <br/>
                                                            <strong>Stock : </strong> {row.stock}
                                                        </td>
                                                        <td>{row.description}</td>
                                                        <td className="text-center">
                                                            <Link to={`/products/edit/${row.id}`} className='btn btn-warning me-2' data-bs-toggle="tooltip" data-bs-placement="bottom" title="Edit">
                                                                <FaEdit/>
                                                            </Link>
                                                            <Button variant="danger" onClick={()=>deleteProduct(row.id)} data-bs-toggle="tooltip" data-bs-placement="bottom" title="Delete">
                                                                <FaTrash/>
                                                            </Button>
                                                        </td>
                                                    </tr>               
                                                ))
                                            )
                                        }
                                    </tbody>
                                </Table>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default ListProduct;
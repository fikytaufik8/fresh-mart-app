import React, {useEffect, useState} from 'react'
import axios from 'axios'
import { useHistory } from 'react-router'
import {Link} from 'react-router-dom'
import {Card, Form, Button, Container, Row, Col} from 'react-bootstrap'
import NavbarComponent from '../../components/NavbarComponent'
import Swal from 'sweetalert2'
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";

function CreateProduct(){
    const history = useHistory();

    const [validation, setValidation] = useState([]);
    const [categories, setCategory] = useState([]);

    const token = localStorage.getItem("token");

    useEffect(()=>{
        fetchData() 
    },[])

    const initialValues = {
        name: '',
        product_category_id: '',
        image: '',
        weight: '',
        price: '',
        stock: '',
        description: ''
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .min(2, "Mininum 2 characters").required('Name is required'),
        product_category_id: Yup.number()
            .integer().required('Product Category is required'),
        image: Yup.mixed()
            .notRequired(),
        weight: Yup.number()
            .integer().min(1).required('Weight Category is required'),
        price: Yup.number()
            .integer().min(1).required('Price Category is required'),
        stock: Yup.number()
            .integer().min(1).required('Stock Category is required'),
        description: Yup.string()
            .required('Description is required')
    });

    const fetchData = async () => {
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get("http://localhost:8000/api/product-categories")
        .then((response) => {
            setCategory(response.data[0]);
        })
    }

    const createProduct = async (e) => {
        e.preventDefault();

        const formData = new FormData()
        
        formData.append('name', e.target.name.value)
        formData.append('product_category_id', e.target.product_category_id.value)
        formData.append('image', e.target.image.files[0])
        formData.append('weight', e.target.weight.value)
        formData.append('price', e.target.price.value)
        formData.append('stock', e.target.stock.value)
        formData.append('description', e.target.description.value)
        console.log(formData)

        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.post(`http://localhost:8000/api/products/create`, formData).then(({data})=>{
            Swal.fire({
                icon:"success",
                text:"Data has been created !"
            })

            history.push('/products');
        
        }).catch(({response})=>{
            if(response.status === 422){
                setValidation(response.data.errors)
            }else{
                Swal.fire({
                    text:response.data.message,
                    icon:"error"
                })
            }
        })
    }

    return(
        <React.Fragment>
            <NavbarComponent />
            <Container>
                <Card className="mt-4 mb-4">
                    <Card.Body>
                        <Row className="mt-4">
                            <h4>
                                Create Product
                            </h4>
                        </Row>
                        <Row>
                            <Col>
                            <Formik 
                                initialValues={initialValues} 
                                validationSchema={validationSchema}
                            >
                            {({ errors, touched, values, handleChange, handleBlur}) => {
                            console.log(values)
                            return (
                                    <Form onSubmit={createProduct}>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Name</Form.Label>
                                            <Form.Control type="text" name="name" value={values.name} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} autoComplete="off"/>
                                            <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Category</Form.Label>
                                            <Form.Select aria-label="Default select example" name="product_category_id" value={values.product_category_id} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.product_category_id && touched.product_category_id ? ' is-invalid' : '')}>
                                                <option>Choose Category</option>
                                                { categories.map((item, key) => (
                                                    <option key={key} value={item.id}>{item.name}</option>
                                                ))}
                                            </Form.Select>
                                            <ErrorMessage name="product_category_id" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Image</Form.Label>
                                            <Form.Control type="file" name="image" onChange={handleChange} value={values.image} onBlur={handleBlur} className={'form-control' + (errors.image && touched.image ? ' is-invalid' : '')}/>
                                            <ErrorMessage name="image" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Weight</Form.Label>
                                            <Form.Control type="number" name="weight" value={values.weight} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.weight && touched.weight ? ' is-invalid' : '')} autoComplete="off"/>
                                            <ErrorMessage name="weight" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Price</Form.Label>
                                            <Form.Control type="number" name="price" value={values.price} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.price && touched.price ? ' is-invalid' : '')} autoComplete="off"/>
                                            <ErrorMessage name="price" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                            <Form.Label>Stock</Form.Label>
                                            <Form.Control type="number" name="stock" value={values.stock} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.stock && touched.stock ? ' is-invalid' : '')} autoComplete="off"/>
                                            <ErrorMessage name="price" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                                            <Form.Label>Description</Form.Label>
                                            <Form.Control as="textarea" rows={3} name="description" value={values.description} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.description && touched.description ? ' is-invalid' : '')}/>
                                            <ErrorMessage name="description" component="div" className="invalid-feedback" />
                                        </Form.Group>
                                        <Form.Group className="mb-3">
                                            <Button variant="success" type="submit">Save</Button>
                                            <Link className='btn btn-danger' to={"/products"} style={{ marginLeft: 2 }}>
                                                Cancel
                                            </Link>
                                        </Form.Group>
                                    </Form>
                                    );
                                }}
                                </Formik>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default CreateProduct;
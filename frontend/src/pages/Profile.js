import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router'
import axios from 'axios'
import {Container, Card, Form} from 'react-bootstrap'
import NavbarComponent from '../components/NavbarComponent'

function Profile(){
    
    const [user, setUser] = useState("");
    const history = useHistory();
    const token = localStorage.getItem("token");

    const fetchData = async () =>{
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get("http://localhost:8000/api/user")
        .then((response) => {
            console.log(response.data)
            setUser(response.data);
        })
    }

    useEffect(() =>{
        if(!token){
            history.push('/');
        }

        fetchData();
    }, []);

    return(
        <React.Fragment>
            <NavbarComponent/>
            <Container>
                <Card className="mt-4">
                    <Card.Header as="h5">Profile</Card.Header>
                    <Card.Body>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label><strong>Name :</strong> {user.name}</Form.Label>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                            <Form.Label><strong>Email :</strong> {user.email}</Form.Label>
                        </Form.Group>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default Profile;
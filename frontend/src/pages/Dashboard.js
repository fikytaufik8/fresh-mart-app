import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router'
import axios from 'axios'
import {Container, Card} from 'react-bootstrap'
import NavbarComponent from '../components/NavbarComponent'

function Dashboard(){
    
    const [user, setUser] = useState("");
    const history = useHistory();
    const token = localStorage.getItem("token");

    const fetchData = async () =>{
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get("http://localhost:8000/api/user")
        .then((response) => {
            setUser(response.data);
        })
    }

    useEffect(() =>{
        if(!token){
            history.push('/');
        }

        fetchData();
    }, []);

    return(
        <React.Fragment>
            <NavbarComponent/>
            <Container>
                <Card className="mt-4">
                    <Card.Body>
                        <Card.Title>Dashboard</Card.Title>
                        <Card.Text>
                            Welcome {user.name} !
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Container>
        </React.Fragment>
    )
}

export default Dashboard;
import React, {useEffect, useState} from 'react'
import {useHistory} from 'react-router'
import {Link} from 'react-router-dom'
import axios from 'axios'
import {Form, Button, Alert} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Formik, ErrorMessage} from "formik";
import * as Yup from "yup";

function Login(){

    const [validation, setValidation] = useState("");

    const history = useHistory();

    useEffect(() =>{
        if(localStorage.getItem('token')){
            history.push('/dashboard');
        }
    }, []);

    const initialValues = {
        email: '',
        password: ''
    };

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email().required('Email is required'),
        password: Yup.string()
            .required('Password is required')
    });

    const loginHandler = async (e) => {
        e.preventDefault();

        const formData = new FormData();

        formData.append('email', e.target.email.value);
        formData.append('password', e.target.password.value);

        await axios.post("http://localhost:8000/api/login", formData)
        .then((response) => {
            localStorage.setItem('token', response.data.token)

            Swal.fire({
                icon:"success",
                text:"Login Success !"
            })

            history.push('/dashboard')
        })
        .catch((error) => {
            setValidation(error.response.data)
        })
    }

    return(
        <div className="container" style={{marginTop: "120px"}}>
            <div className="row justify-content-center">
                <div className="col-md-6">
                    <div className="card border-0 rounded shadow-sm">
                        <div className="card-body">
                            <h4>Login</h4>
                            <Formik 
                                initialValues={initialValues} 
                                validationSchema={validationSchema}
                            >
                            {({ errors, touched, values, handleChange, handleBlur}) => {
                                return (
                                        <Form onSubmit={loginHandler}>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Email</Form.Label>
                                                <Form.Control type="email" name="email" value={values.email} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} autoComplete="off"/>
                                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
                                                <Form.Label>Password</Form.Label>
                                                <Form.Control type="password" name="password" value={values.password} onChange={handleChange} onBlur={handleBlur} className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')}/>
                                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                                            </Form.Group>
                                            <Button variant="success" type="submit">Login</Button>
                                            <Link className='btn btn-primary' to={"/register"} style={{ marginLeft: 2 }}>
                                                Register
                                            </Link>
                                        </Form>
                                );
                            }}
                            </Formik>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Login;
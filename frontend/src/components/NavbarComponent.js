import React, {useEffect, useState} from 'react'
import {Nav, Navbar, NavDropdown, Container} from 'react-bootstrap'
import {useHistory} from 'react-router'
import axios from 'axios'
import Swal from 'sweetalert2'

const NavbarComponent = () =>{

    const [user, setUser] = useState("");
    const history = useHistory();
    const token = localStorage.getItem("token");

    const fetchData = async () =>{
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        await axios.get("http://localhost:8000/api/user")
        .then((response) => {
            setUser(response.data);
        })
    }

    useEffect(() =>{
        if(!token){
            history.push('/');
        }
        fetchData()
    }, []);

    const logoutHanlder = async () =>{
        axios.defaults.headers.common['Authorization'] = 'Bearer' + token

        const isConfirm = await Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, logout!'
        }).then((result) => {
            return result.isConfirmed
        });

        if(!isConfirm){
            return;
        }

        await axios.post("http://localhost:8000/api/logout")
        .then(() => {
            localStorage.removeItem("token")

            history.push('/');
        });
    };

    return(
        <Navbar style={{backgroundColor: "#007D71"}} expand="sm" variant="dark">
            <Container>
                <Navbar.Brand style={{fontFamily: "Arial"}} href="/dashboard">Fresh Mart</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link href="/dashboard">Dashboard</Nav.Link>
                        <Nav.Link href="/products">Product</Nav.Link>
                        <Nav.Link href="/product-categories">Category</Nav.Link>
                    </Nav>
                    <Nav>
                        <NavDropdown title={user.name} id="navbarScrollingDropdown">
                            <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#logout" onClick={logoutHanlder}>
                                Logout
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default NavbarComponent;
import React from 'react'
import {Switch, Route} from 'react-router-dom'
import Register from './pages/Register'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'
import Profile from './pages/Profile'

import ListProductCategory from './pages/product-category/ListProductCategory'
import CreateProductCategory from './pages/product-category/CreateProductCategory'
import EditProductCategory from './pages/product-category/EditProductCategory'

import ListProduct from './pages/product/ListProduct'
import CreateProduct from './pages/product/CreateProduct'
import EditProduct from './pages/product/EditProduct'

function App(){
    return(
        <div>
            <Switch>
                <Route exact path="/" component={Login}></Route>
                <Route exact path="/register" component={Register}></Route>
                <Route exact path="/dashboard" component={Dashboard}></Route>
                <Route exact path="/profile" component={Profile}></Route>

                //product-categories
                <Route exact path="/product-categories" component={ListProductCategory}></Route>
                <Route exact path="/product-categories/create" component={CreateProductCategory}></Route>
                <Route exact path="/product-categories/edit/:id" component={EditProductCategory}></Route>

                //product
                <Route exact path="/products" component={ListProduct}></Route>
                <Route exact path="/products/create" component={CreateProduct}></Route>
                <Route exact path="/products/edit/:id" component={EditProduct}></Route>
            </Switch>
        </div>
    )
}

export default App;
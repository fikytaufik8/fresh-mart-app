<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', 'App\Http\Controllers\AuthController@register');
Route::post('/login', 'App\Http\Controllers\AuthController@login');
Route::post('/logout', 'App\Http\Controllers\AuthController@logout');

Route::get('/product-categories', 'App\Http\Controllers\ProductCategoryController@index');
Route::post('/product-categories/create', 'App\Http\Controllers\ProductCategoryController@create');
Route::get('/product-categories/show/{id}', ['uses' => 'App\Http\Controllers\ProductCategoryController@show']);
Route::post('/product-categories/update/{id}', ['uses' => 'App\Http\Controllers\ProductCategoryController@update']);
Route::delete('/product-categories/delete/{id}', ['uses' => 'App\Http\Controllers\ProductCategoryController@destroy']);

Route::get('/products', 'App\Http\Controllers\ProductController@index');
Route::post('/products/create', 'App\Http\Controllers\ProductController@create');
Route::get('/products/show/{id}', ['uses' => 'App\Http\Controllers\ProductController@show']);
Route::post('/products/update/{id}', ['uses' => 'App\Http\Controllers\ProductController@update']);
Route::delete('/products/delete/{id}', ['uses' => 'App\Http\Controllers\ProductController@destroy']);

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
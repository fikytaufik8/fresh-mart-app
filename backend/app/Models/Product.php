<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $table = "products";
    protected $fillable = [
        'name', 'product_category_id', 'slug', 'image', 'description','weight','stock','price'
    ];

    public function productCategory(){
        return $this->belongsTo(ProductCategory::Class);
    }
}

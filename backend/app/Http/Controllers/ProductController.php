<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\Product;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(){
        // $this->middleware('auth');
    }

    public function index(){
        $data = Product::with('productCategory')->get();

        return response()->json(['status' => 200, $data]);
    }

    public function create(Request $request){
        $this->validate($request, [
            'name'                 => 'required|max:255',
            'product_category_id'  => 'required|max:255',
            'image'                => 'image|max:2048',
            'weight'               => 'required|max:255',
            'stock'                => 'required|max:255',
            'price'                => 'required|max:255',
        ]);

        $input = $request->all();
        $input['slug'] = strtolower(str_replace(' ', '-', $input['name']));

        $fileName = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $input['name']).'-'.time().'.'.$request->image->getClientOriginalExtension()));
        $filePath = 'uploads/products/images/'.$fileName;
        Storage::disk('public')->putFileAs('uploads/products/images', $request->image,$fileName);
        $input['image'] = $filePath;

        $save = Product::create($input);

        if($save){
            $response = response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Create Data Success !",
                                     'data' => $save
                                ]
                ], 200
            );
            return $response;
        }else{
            $response = response()->json(
                [
                    'response' => [
                                    'code' => 400,
                                    'message' => "Create Data Failed !",
                                ]
                ], 400
            );
            return $response;
        }
    }

    public function show($id){
        $data = Product::find($id);

        if($data){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Show Data Success !",
                                     'data' => $data
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Show Data Failed !",
                                ]
                ], 400
            );
        }

    }

    public function update(Request $request,$id){
        $this->validate($request, [
            'name'                 => 'required|max:255',
            'product_category_id'  => 'required|max:255',
            'image'                => 'image|max:2048',
            'weight'               => 'required|max:255',
            'stock'                => 'required|max:255',
            'price'                => 'required|max:255',
        ]);

        $product = Product::find($id);
        if(!$product){
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Data Not Found !",
                                ]
                ], 400
            );
        }
    
        $input = $request->except('_method');

        $product->fill($request->post())->update();
        $product->slug = strtolower(str_replace(' ', '-', $input['name']));

        if($request->hasFile('image')){

            // remove old image
            if($product->image){
                $exists = Storage::disk('public')->exists("uploads/products/images/{$product->image}");
                if($exists){
                    Storage::disk('public')->delete("uploads/products/images/{$product->image}");
                }
            }

            $fileName = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $input['name']).'-'.time().'.'.$request->image->getClientOriginalExtension()));
            $filePath = 'uploads/products/images/'.$fileName;
            Storage::disk('public')->putFileAs('uploads/products/images/', $request->image,$fileName);
            $product->image = $filePath;
            $product->save();
        }

        // $filePath      = '';
        // if($request->hasFile('image')){
        //     if(!empty($request->image)){
        //         File::delete(storage_path($product->image));
        //     }

        //     $file     = $request->file('image');
        //     $fileName = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $input['name']).'-'.time().'.'.$file->getClientOriginalExtension()));
        //     $filePath = 'uploads/products/'.$fileName;
        //     $file     = $request->file('image')->move(storage_path('uploads/products'), $fileName);

        //     $input['image']   = $filePath;
        // }
          
        // $product->update($input);

        if($product){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Update Data Success !",
                                     'data' => $product
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Update Data Failed !",
                                ]
                ], 400
            );
        }
    }

    public function destroy($id){
        $product = Product::find($id);
        if(!$product){
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Data Not Found !",
                                ]
                ], 400
            );
        }

        if($product->image){
            $exists = Storage::disk('public')->exists($product->image);
            if($exists){
                Storage::disk('public')->delete($product->image);
            }
        }

        $product->delete();

        if($product){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Delete Data Success !",
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Delete Data Failed !",
                                ]
                ], 400
            );
        }
    }
}

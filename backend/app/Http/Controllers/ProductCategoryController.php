<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use App\Models\ProductCategory;
use App\Imports\ProductCategoryImport;
use App\Exports\ProductCategoryExport;
use Excel;

class ProductCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(){
        // $this->middleware('auth');
    }

    public function index(){
        // dd('yes');
        $data = ProductCategory::all();

        return response()->json(['status' => 200, $data]);
    }

    public function create(Request $request){
        $this->validate($request, [
            'name'         => 'required|max:255',
        ]);

        $input = $request->all();
        $input['slug'] = strtolower(str_replace(' ', '-', $input['name']));

        $save = ProductCategory::create($input);

        if($save){
            $response = response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "succcess",
                                     'message' => "Create Data Success !",
                                     'data' => $save
                                ]
                ], 200
            );
            return $response;
        }else{
            $response = response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Create Data Failed !"
                                ]
                ], 400
            );
            return $response;
        }
    }

    public function show($id){
        $data = ProductCategory::find($id);

        if($data){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Show Data Success !",
                                     'data' => $data
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Show Data Failed !",
                                ]
                ], 400
            );
        }

    }

    public function update(Request $request,$id){
        $this->validate($request, [
            'name'         => 'required|max:255',
        ]);
    
        $input = $request->all();
        $input['slug'] = strtolower(str_replace(' ', '-', $input['name']));
          
        $category = ProductCategory::find($id);
        if(!$category){
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Data Not Found !",
                                ]
                ], 400
            );
        }
          
        $category->update($input);

        if($category){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Update Data Success !",
                                     'data' => $category
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Update Data Failed !",
                                ]
                ], 400
            );
        }
    }

    public function destroy($id){
        $delete = ProductCategory::find($id)->delete();

        if($delete){
            return response()->json(
                [
                    'response' => [
                                     'code' => 200,
                                     'status' => "success",
                                     'message' => "Delete Data Success !",
                                ]
                ], 200
            );
        }else{
            return response()->json(
                [
                    'response' => [
                                     'code' => 400,
                                     'status' => "failed",
                                     'message' => "Delete Data Failed !",
                                ]
                ], 400
            );
        }
    }

    // public function export(){
    //     return Excel::download(new ProductCategoryExport, 'Export Product Category.xlsx');

    // }

    // public function import(Request $request){
    //     $this->validate($request, [
    //         'file'  => 'required'
    //     ]);

    //     if($request->hasfile('file')){
    //         $file = 'Product Category Import.'. $request->file('file')->getClientOriginalExtension();
    //         $request->file('file')->move('uploads/excel/',$file);
    //         Excel::import(new ProductCategoryImport, 'uploads/excel/'.$file);
    //     }
        

    //     return response()->json(['success'],200);
    // }
}
